#include <stdio.h> 
#include <sys/stat.h>

int main()
{
    char str[20];
    FILE *f1=fopen("directory/file.txt","r");
    FILE *f2=fopen("directory/file2.txt","w");
    fgets(str,20,f1);
    fputs(str,f2);
    printf("Enter text: ");
    gets(str);
    fputs(str,f2);
    fputs("\n",f2);
    while(fgets(str,20,f1))
    {
        fputs(str,f2);
    }
    fclose(f1);
    fclose(f2);
    remove("directory/file.txt");
    rename("directory/file2.txt","directory/file.txt");
}