#include<stdio.h>

void func1(char fname[]);
void func2(char fname[]);

int main()
{
    printf("Function 1:\n-----\n");
    func1("Prog1.c");
    printf("\n-----\nFunction 2:\n-----\n");
    func2("Prog1.c");
    printf("\n-----\n");
}

void func1(char fname[])
{
    char str[20];
    FILE *f=fopen(fname,"r");
    while(1)
    {
        fgets(str,20,f);
        printf("%s",str);
        if(feof(f)) break;
    }
    fclose(f);
}

void func2(char fname[])
{
    char str[20];
    FILE *f=fopen(fname,"r");
    while(fgets(str,20,f))
    {
        printf("%s",str);
    }
    fclose(f);
}