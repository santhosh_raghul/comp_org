FileName: .asciz "numbers.txt"
.align
FH: .word 0
str: .skip 80

ldr r0,=FileName
mov r1,#0
swi 0x66
ldr r1,=FH
str r0,[r1]
ldr r1,=str
mov r2,#80
swi 0x6a
ldr r4,=str

Loop:
ldrb r2,[r4],#1
cmp r2,#0
beq End
cmp r2,#0x2c
beq newline
mov r0,r2
swi 0x00
bal Loop

newline:
mov r0,#'\n'
swi 0x00
bal Loop

End:
ldr r0,=FH
ldr r0,[r0]
swi 0x68
swi 0x11