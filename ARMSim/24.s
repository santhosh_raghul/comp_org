.equ Scount, 250
.equ Lcount, 2000
.equ cycles, 3

BULB_pattern:
  mov r3,#cycles  
  Loop:
    mov r0,#2
    swi 0x201
    mov r4,pc
    B Ltimer
    mov r0,#1
    swi 0x201
    mov r4,pc
    B Ltimer
    subs r3,r3,#1
    BNE Loop

mov r0,#0
swi 0x201
swi 0x11 

Ltimer:
    mov r2,#Lcount
   Lloop:
      BL Stimer       @branch with link
      subs r2,r2,#1   @r2=r2-1
      BNE Lloop 
      mov pc,r4          

Stimer:
     mov r1,#Scount
Sloop:
     subs r1,r1,#1     @r1=r1-1 s at end of sub updates condition flag         
     BNE Sloop
     mov PC,LR         @put the value at link register to program counter
     


   