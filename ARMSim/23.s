.equ outcount, 500
.equ incount, 100
  
Outer_Timer:
  mov r2,#outcount
  Outer_Loop:
    BL Inner_Timer       @branch with link
    subs r2,r2,#1
    BNE Outer_Loop
  BL Printa
  BAL Outer_Timer

swi 0x11            

Inner_Timer:
  mov r1,#incount
  Inner_Loop:
    subs r1,r1,#1      
    BNE Inner_Loop
    mov PC,LR         @put the value at link register to program counter

Printa:
  mov r0,#'a'
  swi 0x00
  mov r0,#'\n'
  swi 0x00
  mov PC,LR         @put the value at link register to program counter
