FileName: .asciz "comp-lines.txt"
eq: .asciz "Equal"
neq: .asciz "Not equal"
.align
FH: .word 0
line1: .skip 80
line2: .skip 80

ldr r0,=FileName
mov r1,#0
swi 0x66
ldr r1,=FH
str r0,[r1]

ldr r1,=line1
mov r2,#80
swi 0x6a
ldr r4,=line1

ldr r1,=line2
mov r2,#80
swi 0x6a
ldr r5,=line2

compare:
ldrb r1,[r4],#1
ldrb r2,[r5],#1
cmp r2,r1
bne noteq

ldr r1,=eq
mov r2,#80
swi 0x6a
ldr r0,=FH
ldr r0,[r0]
swi 0x68
swi 0x11

noteq:
ldr r1,=neq
mov r2,#80
swi 0x6a
ldr r0,=FH
ldr r0,[r0]
swi 0x68
swi 0x11
